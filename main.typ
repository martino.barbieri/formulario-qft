#import "packs.typ": *

#show: doc.with(
  title     : "Formulario di QFT",
)

#set heading(outlined: false)
#set heading(bookmarked: true)

= Relatività
$
  i = 1,2,3 wide mu = 1,2,3,0
$$
  eta_(mu nu) = mat(bb(1)_3, 0; 0, -1)
$$
  epsilon^(0123)=epsilon_(1230)=+1=-epsilon_(0123)
$$
  square = diff^mu diff_mu = nabla^2 - diff_t^2 = E^2 - P^2
$
Trick:
$
  epsilon^(mu_1 dots mu_(D-1)nu)epsilon_(mu_1 dots mu_(D-1) rho) = c dot delta^nu_rho
$
e $c$ si determina prendendo la traccia: $c = 1/D tr[epsilon^(mu_1 dots mu_(D))epsilon_(mu_1 dots mu_(D))]$.\
La misura Lorentz-invariante è (dim: integra $delta(p^2)theta(p^0)$)
$
  dd(p,3)/(2 E_vb(p)(2 pi)^3)
$

== Tensori antisimmetrici
#{
set math.mat(column-gap: 1em)
$
  vecrow(vb(v),vb(w))^(mu nu)=mat(-vb(w)wedge,-vb(v);vb(v),0)=mat(epsilon_(mu nu k)w_k,-v^nu;v^mu,0) wide
  vecrow(vb(v),vb(w))_(mu nu)=mat(-vb(w)wedge,vb(v);-vb(v),0)=mat(epsilon_(mu nu k)w_k,v^nu;-v^mu,0)
$}$
  tr[vb(vecrow(v,w)) vb(vecrow(s,t))] = -vb(vecrow(v,w))^(mu nu) vb(vecrow(s,t))_(mu nu) = -2vb((v dot s - w dot t))
$$
  wide det vecrow(vb(v),vb(w)) = epsilon_(mu nu rho sigma)
  vecrow(vb(v),vb(w))^(1 mu)vecrow(vb(v),vb(w))^(2 nu)vecrow(vb(v),vb(w))^(3 rho)vecrow(vb(v),vb(w))^(0 sigma)
  = (vb(v)dot vb(w))^2
$$
  v_i = vb(vecrow(v,w))_(i 0) = vb(vecrow(v,w))^(0i) wide w_i=1/2 epsilon_(i j k)vb(vecrow(v,w))_(j k) wide
  vb(vecrow(v,w))_(i j) = epsilon_(i j k) w_k
$$
  vb(vecrow(w,-v))^(mu nu) = 1/2 epsilon^(mu nu sigma rho) vb(vecrow(v,w))_(sigma rho)
$

== Trasformazioni di Lorentz
$
  x^mu mapsto tensor(Lambda,+mu,-nu) x^nu - a^mu wide T(Lambda,a)T(Lambda',a') = T(Lambda Lambda', a + Lambda a')
$
Condizione di *pseudo-ortogonalità*: $Lambda^T eta Lambda = eta arrow.double Lambda^(-1)=eta Lambda^T eta arrow.double det Lambda = pm 1$\
Boost passivo standard ortocrono:
#set math.mat(column-gap: 1.5em)
$
tensor(Lambda,+mu,-nu) = mat(
  gamma, -vb(beta) gamma;
  -vb(beta)gamma, bb(1)+(gamma-1)hat(vb(beta)) times.circle hat(vb(beta))
)
$
$
  dd(s)^2 = cases(
    "time-like":  &dd(s)^2<0\
    "light-like": &dd(s)^2=0\
    "space-like": &dd(s)^2>0
  )
$
Generica trasformazione: prodotto di
+ trasformazione ortocrona ($tensor(Lambda,+0,-0)>=0$ e $det Lambda = +1$)
+ trasformazioni discrete $display(cal(P) = mat(-bb(1),0;0,1))$ e $display(cal(T) = mat(bb(1),0;0,-1))$
=== Algebra di Poincaré
Sulle coordinate, il gruppo di Poincaré induce una rappresentazione affine. Per ridurci a una rappresentazione si nota che a una rappresentazione affine in $D$ dimensioni
si può associare una rappresentazione proiettiva in $D+1$ dimensioni:
$
  x^mu mapsto vec(x^mu,1) wide “thin tensor(Lambda,+mu,-nu)x^nu - a^mu thin” mapsto mat(Lambda,-a^mu;0,1)
$
Gli operatori qui sotto sono da intendersi in tale senso.
Per una trasformazione infinitesima, sulle coordinate ($omega_(sigma rho)$ antisimmetrica per pseudo-ortogonalità)
$
  x^mu mapsto tensor([e^(i/2 omega_(rho sigma) J^(rho sigma))],+mu,-nu) x^nu - a^mu
  simeq x^mu + i/2 omega_(rho sigma) tensor(i[eta^(mu sigma)tensor(delta,+rho,-nu)-eta^(mu rho)tensor(delta,+sigma,-nu)],+mu,-nu) +i a^nu (i tensor(delta,+mu,-nu))
$
da cui, per lo spazio delle coordinate
$
  J^(rho sigma) = i{eta^(mu sigma)tensor(delta,+rho,-nu)-eta^(mu rho)tensor(delta,+sigma,-nu)} wide P^mu = i eta^(mu nu)
$
e infine otteniamo l'algebra di Poincaré:
$
  i[J^(mu nu),J^(rho sigma)]=eta^(nu rho) J^(mu sigma)-eta^(nu sigma) J^(mu rho)-eta^(mu rho) J^(nu sigma)+eta^(mu sigma) J^(nu rho) \
  i[P^mu,J^(rho sigma)]=eta^(mu rho) P^sigma - eta^(mu sigma) P^rho wide [P^mu,P^nu] = 0
$
Le trasformazioni saranno *passive*:
$
  omega = (-vb(chi),vb(theta)) wide gamma = cosh chi wide beta = tanh chi
$
La rappresentazione aggiunta è data da
$
  U(Lambda,a) J^(rho sigma) U^(-1)(Lambda,a) = tensor(Lambda,-mu,+rho)tensor(Lambda,-nu,+sigma)(J^(mu nu)+a^mu P^nu - a^nu P^mu) \
  U(Lambda,a) P^rho U^(-1)(Lambda,a) = tensor(Lambda,-mu,+rho) P^mu
$
In componenti tridimensionali $J=vb(vecrow(K,J))$:
$
  P^i wide H=P^0 wide J^i = 1/2 epsilon_(i j k) J^(j k) wide K^i = J^(0 i)
$$
  [J_i,J_j]=i epsilon_(i j k) J_k wide [J_i,K_j] = i epsilon_(i j k) K_k wide [K_i, K_j] = -i epsilon_(i j k) J_k \
  [J_i, P_j]=i epsilon_(i j k) P_k wide [K_i, P_j] = -i H delta_(i j) wide [J_i, H] = [P_i, H] = [H,H] = 0 wide [K_i, H] = -i P_i
$
Le Lorentz omogenee si scrivono
$
  tensor(Lambda,+mu,-nu)
  = tensor(eval(e^(i/2 omega_(rho sigma) J^(rho sigma))),+mu,-nu)
  = tensor(eval(e^(i(vb(theta dot J + chi dot K)))),+mu,-nu)
  = tensor(eval(e^(i U_i ( theta_i - i phi.alt_i) + i V_i ( theta_i + i phi.alt_i))),+mu,-nu)
$
$
  vb(theta)_L = vb(theta) - i vb(chi) wide vb(theta)_R = vb(theta) + i vb(chi)
$
$
cases(
  U_i = 1/2 (J_i+i K_i),
  V_i = 1/2 (J_i-i K_i)
)
wide
cases(
  J_i = V_i + U_i,
  K_i = i(V_i-U_i)
)
$
danno un isomorfismo $frak("so")(3,1) tilde.equiv frak("su")(2) times frak("su")(2)$ ($[U_i,V_j]=0$).

Contrazione di Inönü-Wigner ($vb(chi dot) K simeq vb(v) dot vb(K)/c arrow.double vb(K)^"Gal"=1/c vb(K)$ e $H=P^0=M c+W/c$):
$
  [J_i,J_j]=i epsilon_(i j k) J_k wide [J_i,K^"Gal"_j] = i epsilon_(i j k) K^"Gal"_k wide [K^"Gal"_i, K^"Gal"_j] = 0 wide [thin dot thin,M]=0\
  [J_i, P_j]=i epsilon_(i j k) P_k wide [K^"Gal"_i, P_j] = -i M delta_(i j) wide [J_i, W] = [P_i, W]  = 0 wide [K^"Gal"_i, W] = -i P_i
$
Usando CBHW ($e^X e^Y=e^(X+Y+1/2 [X,Y]+1/12 [X,[X,Y]]-[Y,[X,Y]]+ ...)$)
$
  e^(-i vb(v)dot vb(K)^"Gal")e^(-i vb(a)dot vb(P)) = e^(i/2 M vb(v dot a)) e^(-i(vb(v)dot vb(K)^"Gal"+vb(a)dot vb(P)))
$
- o introduciamo una regola di superselezione sulla massa...
- ...o allarghiamo il gruppo di Galileo.

== Tempo euclideo e Wick rotation
$
  x^4 = i x^0        &wide x_4 = -i x_0 \
  x^0 mapsto -i x^4  &wide x_0 mapsto i x_4
$

= Meccanica Quantistica
== Postulati
+ Gli stati sono elementi di $bb(P)cal(H)$.
+ A ogni osservabile è associato un operatore autoaggiunto.
+ Regola di Born: $P(cal(R mapsto R'))=|cal(braket(R,R'))|^2$.
== Simmetria
$
  S: bb(P)cal(H)arrow bb(P)cal(H')" s.t. " P (cal(R)_i mapsto cal(R)_j)=P (cal(R)'_i mapsto cal(R)'_j)
$
e s.t. $S$ commuta con $+$ e con la moltiplicazione per scalare reale.
=== Teorema di Wigner
Ogni $S$ è sollevabile a una mappa unitaria o antiunitaria $cal(H) arrow cal(H)'$.

L'aggiunto di un operatore antilineare è definito da $braket(phi,A^dagger psi)=braket(psi, A phi)=braket(A phi, psi)^*$.
== Teoria dei Gruppi
Sia $cal(G)$ il gruppo di simmetria del sistema. $T_1, T_2 in cal(G)$. $cal(G)$ induce un'azione su $bb(P)cal(H)$ che,
allargando il gruppo, si può sollevare a rappresentazione sull'intero $cal(H)$.
$
  U(T_2)U(T_1) = e^(i phi(T_1,T_2)) U(T_2 T_1)
$
La fase non dipende dai vettori a meno di regole di superselezione. In tal caso, dipende solo dalla classe di equivalenza
degli stati.
=== Gruppi di Lie
$T(theta)T(theta')=T(f(theta,theta'))$ con $f(theta,0)=f(0,theta)=theta$ e $T(0)=bb(1)$.

Al secondo ordine deve essere $f^a (theta,theta') simeq theta^a+theta'^a+tensor(f,+a,-b,-c) theta^b theta'^c$.

Per una rappresentazione $U(T(theta)) simeq bb(1) + i theta^a t_a - 1/2 theta^b theta^c t_(b c)$ con $t_(b c)$ simmetrico.
Segue
$
  t_(b c) = t_b t_c +i tensor(f,+a,-b,-c)t_a \
  [t_b,t_c] = i tensor(C,+a,-b,-c) t_a " con " tensor(C,+a,-b,-c) = -tensor(f,+a,-b,-c)+tensor(f,+a,-c,-b)
$

== Stati a singola particella
Scomponiamo in irreps usando le *rappresentazioni indotte* (irreps #math.arrow.double posso ottenere tutto da un solo vettore):
+ diagonalizziamo il sottogruppo abeliano normale generato da $P^mu$. Chiamiamo $sigma$ gli altri dof (che supponiamo discreti)
 $
  P^mu Psi_(p,sigma) = p^mu Psi_(p,sigma) wide U(bb(1),a) Psi_(p,sigma) = e^(i a dot p) Psi_(p,sigma)
 $
+ definiamo i _boost standard_ $L(p)$ e i _momenti standard_ $k^mu$: $Psi_(p,sigma)=sqrt(k^0/p^0) U(L(p)) Psi_(k,sigma)$
  #table(
    columns: (2fr,1fr,1fr),
    [],                    [*Standard $k^mu$*],        [*Little Group*],
    $p^2<0 quad p^0 > 0$,  $(0,0,0,M)$,                $"SO"(3)$,
    $p^2<0 quad p^0 < 0$,  $(0,0,0,-M)$,               $"SO"(3)$,
    $p^2=0 quad p^0 > 0$,  $(0,0,kappa,kappa)$,        $"ISO"(2)$,
    $p^2=0 quad p^0 < 0$,  $(0,0,kappa,-kappa)$,       $"ISO"(2)$,
    $p^2>0$,               $(0,0,N,0)$,                $"SO"(2,1)$,
    $p^mu=0$,              $(0,0,0,0)$,                $"SO"(3,1)$,
  )
+ analizziamo il sottogruppo di Wigner (_little group_: $tensor(W,+mu,-nu)k^nu=k^mu$):
 $
  W(Lambda,p) = L^(-1)(Lambda p) Lambda L(p) wide W(Lambda,p) Psi_(k,sigma) = D(Lambda,p)_(sigma sigma') Psi_(k,sigma') \
 $
+ in generale
 $
  U(Lambda) Psi_(p,sigma) = sqrt((Lambda p)^0/p^0) D(Lambda,p)_(sigma,sigma') Psi_(Lambda p, sigma')
 $
+ $U$ è unitaria nello spazio degli stati (la normalizzazione arriva dalla densità di stati). Prendiamo come
 convenzioni
 $
  (Psi_(p',sigma'),Psi_(p,sigma))=delta^3(p'-p) delta_(sigma' sigma) wide D^dagger = D^(-1)
 $
$"SO"(n,1)$ non hanno rappresentazioni finito-dimensionali unitarie, ma non sono casi fisicamente rilevanti.
I casi con energia negativa si possono ricondurre agli analoghi a energia positiva.
=== Massa positiva
Boost standard (attivo):
$
 tensor(L,+mu,-nu)(p)=mat(gamma, vb(beta) gamma=hat(vb(p))sqrt(gamma^2-1);vb(beta) gamma=hat(vb(p))sqrt(gamma^2-1),bb(1)+(gamma-1)vb(hat(p) otimes hat(p)))
$
Si nota che
$
  W(cal(R),p) = cal(R)
$
quindi volendo potremmo scrivere ogni boost standard come rotazione+boost lungo $z$+rotazione.
=== Massa nulla

= Elettrodinamica
$
  J^mu = vec(rho, vb(J)), #h(1em) A^mu =vec(phi.alt, vb(A))\
  diff_mu J^mu = - diff_mu diff_nu F^(mu nu) = 0
$
$
  F_(mu nu)=diff_mu A_nu - diff_nu A_mu wide F=dd(A) wide F=vb(vecrow(E,B)) \
  tilde(F)^(mu nu) = 1/2 epsilon^(mu nu rho sigma)F_(rho sigma)
$
Equazioni di Maxwell:
$
diff_mu F^(mu nu)+J^nu=0 wide
underbrace(diff_mu tilde(F)^(mu nu)=0,"Identità di Bianchi")
$
Interazione con le cariche: $m derivative(u^mu,tau)-q/c u_nu F^(mu nu)=0$

In $cal(L)$ possiamo aggiungere i termini:
- $F^(mu nu)F_(mu nu)=-2(vb(E)^2-vb(B)^2)$
- $tilde(F)^(mu nu)tilde(F)_(mu nu) = -F^(mu nu)F_(mu nu)$
- il termine topologico:
$
tilde(F)^(mu nu) F_(mu nu)&=
  2 diff_mu (epsilon^(mu nu rho sigma) A_nu diff_rho A_sigma)=\
  &=diff_mu underbrace((epsilon^(mu nu rho sigma) A_nu F_(rho sigma)),
  "termine di Chern-Simons")
  =-4vb(E) dot vb(B)
$
Prendiamo quindi
$
cal(S)=integral dd(x,4) thin { -1/4 F^(mu nu)F_(mu nu)+J_mu A^mu }
$
Ci sono due polarizzazioni indipendenti. Fissando una gauge, per esempio quella di Lorenz,
la terza polarizzazione è di pura gauge:
$
  A^mu = A vec(1,0,0,1)e^(-i k_0 x^0+i k_0 x^3)=A/(i k_0) diff^mu e^(-i k_0 x^0+i k_0 x^3)
$

== Teoria di Proca
$
cal(S)=integral dd(x,4) thin { -1/4 F^(mu nu)F_(mu nu) - m^2/2 A_mu A^mu
+ J_mu A^mu }
$
$
diff_mu F^(mu nu)-m^2 A^nu+ J^nu= 0
$
Prendendone la divergenza, se la corrente è conservata, otteniamo la condizione di Lorenz: $diff_mu A^mu = 0$. Dunque
$
 (square-m^2)A^mu - J^mu = 0
$
In particolare, ci sono tre polarizzazioni che soddisfano le leggi del moto.

Con una carica puntiforme nell'origine ($lambda_c=hbar/(m c)$):
$
A^0 = q/(4pi r) e^(-m r) = q/(4 pi r) e^(-r\/lambda_c)
$

= Spinori $bb((1/2,1/2))$
== Notazione di Dirac
$
  {gamma_mu, gamma_nu} = 2 eta_(mu nu) wide gamma_5 = i gamma_0 gamma_1 gamma_2 gamma_3 = gamma_1 gamma_2 gamma_3 gamma_4
  wide beta = i gamma_0
$
$
  overline(u) = u^dagger beta
$

#line(length: 100%)
*appunti vecchi: questa roba verrà migrata sopra la linea delle cose già sistemate #math.arrow.t*

$
sigma^mu = vec(bb(1)_2,sigma^i) wide overline(sigma)^mu = vec(bb(1)_2, -sigma^i)
wide {sigma^mu, overline(sigma)^nu} = eta^(mu nu)
$
*Pseudorealità del doppietto*: (vale anche con trasposta)
$
  sigma^2 sigma^(i,*) sigma^2 = -sigma^i wide sigma^2 sigma^(mu,*) sigma^2 = overline(sigma)^mu
  wide sigma^2 overline(sigma)^(mu,*) sigma^2 = sigma^mu
$
Causalità: $x dot y < 0$ implica $[phi (x),phi (y)]_(plus.minus)=0$\

=== Momento angolare
$
  e^(i/2 omega^(mu nu)J_(mu nu)) = e^(i (phi.alt_i K_i+theta_i J_i))
$

== Spinori
Usiamo trasformazioni *passive*.
Troviamo le rappresentazioni irriducibili di $op("SO")(1,3)$.
Le rappresentazioni sono classificate dagli autovalori $j_u, j_v$ degli operatori di Casimir $U^2, V^2$.
Uso la convenzione $(j_L, j_R):=(j_u, j_v)$
=== Caso scalare $(bb(0),bb(0))$
È la rappresentazione banale.
=== Caso vettoriale $(bb(1/2),bb(1/2))$
Il momento angolare, come generatore della rappresentazione in forma matriciale, è
$
  tensor(Lambda,+mu,-nu) tilde.eq tensor(bb(1),+mu,-nu)+
  mat(
    0,-vb(beta); -vb(beta), -vb(theta) wedge
  ) simeq tensor(bb(1),+mu,-nu) + tensor(omega,+mu,-nu)
  simeq tensor(bb(1),+mu,-nu) + i mat(
    0,i beta_1, i beta_2, i beta_3;
    i beta_1,0,-i theta_3,i theta_2;
    i beta_2,i theta_3,0,-i theta_1;
    i beta_3,-i theta_2,i theta_1,0
  )
$
I generatori $U, V$ sono
$
  U_1 = 1/2 mat(
    0,-1,0,0;
    -1,0,0,0;
    0,0,0,-i;
    0,0,i,0
  ) wide&
  V_1 = 1/2 mat(
    0,1,0,0;
    1,0,0,0;
    0,0,0,-i;
    0,0,i,0
  )
  \
  U_2 = 1/2 mat(
    0,0,-1,0;
    0,0,0,i;
    -1,0,0,0;
    0,-i,0,0
  ) wide&
  V_2 = 1/2 mat(
    0,0,1,0;
    0,0,0,i;
    1,0,0,0;
    0,-i,0,0
  )
  \
  U_3 = 1/2 mat(
    0,0,0,-1;
    0,0,-i,0;
    0,i,0,0;
    -1,0,0,0
  ) wide&
  V_3 = 1/2 mat(
    0,0,0,1;
    0,0,-i,0;
    0,i,0,0;
    1,0,0,0
  )
$

Vediamo ora che questa corrisponde proprio al caso $bb(1/2)otimes bb(1/2)$: facciamo l'identificazione
$
  A^mu arrow.l.r.double A^mu sigma_mu = A^mu overline(sigma)^mu
$
troviamo gli isomorfismi di rappresentazioni
$
  U_i A^mu arrow.l.r.double sigma^i/2(A^mu sigma_mu) \
  V_i A^mu arrow.l.r.double -(A^mu sigma_mu) sigma^i/2 arrow.l.r.double sigma^i /2 (A^mu overline(sigma)_mu)
$
da cui è naturale chiamare $U_i$ i generatori delle trasformazioni $L$ e $V_i$ quelli delle trasformazioni $R$.
=== Campi $(bb(1),bb(0))$ e $(bb(0),bb(1))$
Con l'azione
$
  F^(mu nu) mapsto tensor(Lambda,+mu,-rho)tensor(Lambda,+nu,-sigma)F^(rho sigma)\
  F mapsto Lambda F Lambda^T
$
si vede che possiamo scomorre $F$ in due sottospazi invarianti:
$
  F_L=mat(
    0,i x_L,i y_L,i z_L;
    -i x_L,0,-z_L,y_L;
    -i y_L,z_L,0,-x_L;
    -i z_L,-y_L,x_L,0
  ) quad oplus quad
  F_R=mat(
    0,-i x_R,-i y_R,-i z_R;
    i x_R,0,-z_R,y_R;
    i y_R,z_R,0,-x_R;
    i z_R,-y_R,x_R,0
  )
$
Al primo ordine
$
  Lambda F Lambda^T simeq F+
    i (vb(theta)_L dot vb(U)+vb(theta)_R dot vb(V)) F +
    i F (vb(theta)_L dot vb(U)+vb(theta)_R dot vb(V))^T
$
Con qualche conto, otteniamo gli ismorfismi di algebre
$
    (vb(theta)_L dot vb(U)+vb(theta)_R dot vb(V)) F +
    F (vb(theta)_L dot vb(U)+vb(theta)_R dot vb(V))^T arrow.l.r.double
    vb(theta)_L dot vb(J)_(bb(1)) dot vb(x)_L +
    vb(theta)_R dot vb(J)_(bb(1)) dot vb(x)_R
$
Notiamo che
- l'operatore $D$ di dualità elettromagnetica ha autovettori $plus.minus i$ ($D^2=-bb(1)$);
- i campi $L$, $R$ sono quelli autoduali (a meno di $plus.minus i$). Più precisamente
  - per le componenti $L$ vale $D F = +i F$;
  - per le componenti $R$ vale $D F = -i F$.
=== Campi $(bb(1/2),bb(0))$ e $(bb(0),bb(1/2))$
Nessuna delle due può descrivere lo spinore di Pauli, siccome questo è invariante sotto parità.
#grid(
  columns: (1fr,1fr), [
    *Spinore left $bb((1/2,0))$*
    $
      Psi_L mapsto e^(i vb(sigma)/2 dot ( vb(theta)-i vb(phi.alt) ) ) Psi_L
    $
  ],
  [
    *Spinore right $bb((0,1/2))$*
    $
      Psi_R mapsto e^(i vb(sigma)/2 dot ( vb(theta)+i vb(phi.alt) ) ) Psi_R
    $
  ]
)

== Trasformazioni discrete
#grid(columns: (1fr,1fr),[
  Parità: unitaria
  $
    cal(P)=cases(vb(x)mapsto-vb(x),t mapsto t)\
    vb(P) mapsto -vb(P) wide H mapsto H\
    vb(J) mapsto vb(J) wide vb(K) mapsto -vb(K)\
    therefore vb(U) arrow.l.r vb(V)\
    vec(E,vb(p),sigma_z) mapsto vec(E,-vb(p),sigma_z)
  $
],[
  Time-reversal: antiunitaria ($p_mu =i diff_mu$)
  $
    cal(T)=cases(vb(x)mapsto vb(x),t mapsto -t)\
    vb(P) mapsto -vb(P) wide H mapsto H\
    vb(J) mapsto -vb(J) wide vb(K) mapsto vb(K)\
    therefore vb(U) arrow.l.r vb(V)\
    vec(E,vb(p),sigma_z) mapsto vec(E,-vb(p),-sigma_z)
  $
])

== Tracciologia
Algebra di Clifford:
$
  {gamma^mu,gamma^nu}=2 eta^(mu nu) bb(1)_4
$
Qualche definizione e qualche proprietà:
$
  slashed(v) = gamma_mu v^mu \
  gamma^(mu nu) = cases(
    0 & "se " mu = nu,
    gamma^mu gamma^nu & "altrimenti"
  ) = 1/2 [gamma^mu, gamma^nu]\
  gamma^(mu nu rho) = cases(
    0 & "se ci sono due indici uguali",
    gamma^mu gamma^nu gamma^rho & "altrimenti"
  ) \
  gamma^(mu nu rho sigma) = cases(
    0 & "se ci sono due indici uguali",
    gamma^mu gamma^nu gamma^rho gamma^sigma & "altrimenti"
  ) =-i epsilon^(mu nu rho sigma) gamma_5 \
  {gamma_5, gamma^mu} = 0 wide {gamma_5, gamma_5}=2 bb(1) \
  beta gamma^mu beta = (gamma^mu)^dagger wide beta gamma^(mu nu) beta = (gamma^(nu mu))^dagger wide
    dots wide (beta^2=bb(1)) \
  [gamma^(mu nu),gamma^rho] = 2(eta^(nu rho) gamma^mu - eta^(mu rho) gamma^nu)
$
Base relativistica:
$
  gamma^mu = mat(0, overline(sigma)^mu; sigma^mu,0) wide beta = mat(0,bb(1);bb(1),0)
  wide gamma_5 = mat(bb(1),0;0,-bb(1))\
  cases(
    gamma^0 = sigma_1 otimes bb(1),
    gamma^i = -i sigma_2 otimes bb(sigma)^i,
    gamma_5 = sigma_3 otimes bb(1)
  )
$

Base non relativistica:
$
  gamma^0 = mat(bb(1), 0; 0,-bb(1)) wide
  gamma^i = mat(0, overline(sigma)^i; sigma^i,0) wide
  beta = mat(0,bb(1);bb(1),0)
  wide gamma_5 = mat(0,-bb(1);-bb(1),0)\
  cases(
    gamma^0 = sigma_3 otimes bb(1),
    gamma^i = -i sigma_2 otimes bb(sigma)^i,
    gamma_5 = -sigma_1 otimes bb(1)
  )
$
(la base non relativistica si ottiene da quella relativistica per una rotazione generata da $sigma^2 otimes bb(1)$.)

== Formule di passaggio al continuo
$
  vb(p)_(vb(n))=(2pi)/L vb(n) wide arrow.double wide
  sum_(vb(k)) mapsto V/(2pi)^3 integral dd(vb(p),3)\
  integral dd(vb(x),3)e^(i vb(p)vb(x))=(2pi)^3 delta(vb(p))=V delta_vb(p) wide arrow.double wide
  delta_vb(p) mapsto (2pi)^3/V delta(vb(p))
$

== Formalismo canonico
E.-L.:
$
  diff_mu (pdv(cal(L),diff_mu Phi_i))=pdv(cal(L),Phi_i)
$
Teorema di Noether ($delta Phi_i = delta_0 Phi_i + delta x^mu diff_mu Phi_i$):
$
delta cal(S) = integral dd(x,4)
{diff_mu (cal(L) delta x^mu+pdv(cal(L),diff_mu Phi_i)delta_0 Phi_i)}\
J^mu=delta x^nu (
  pdv(cal(L),diff_mu Phi_i) diff_nu Phi_i-delta^mu_nu cal(L)
)-pdv(cal(L),diff_mu Phi_i)delta Phi_i
$
$
  diff_mu J^mu =0+"termini di anomalia+termini di contatto" O(hbar)
$
Tensore di Energia-Impulso (conservato rispetto all'indice a sinistra):
$
  T^(mu nu) = pdv(cal(L),diff_mu Phi_i) diff^nu Phi_i-eta^(mu nu) cal(L)
$
Se è trace-less, la teoria ha invarianza conforme.
== Meccanismo di (Nambu-)Goldstone
Per ogni generatore continuo rotto, c'è un bosone massless.

== Campo scalare reale
Equazione del moto (Klein-Gordon):
$
square phi+((m c)/hbar)^2 phi=0
$
$
cal(S)=integral dd(x,4) thin { 1/2 diff_mu phi thin diff^mu phi
  - 1/2 ((m c)/hbar)^2 phi^2}
$
$
pi(vb(x),t)=dv(L, phi(vb(x),t), d: delta)=dot(phi)(vb(x),t)\
[phi(vb(x),t),pi(vb(y),t)]=i delta(vb(x)-vb(y))
$
Pittura di Schröedinger: $Psi[phi]$ funzionale. Pittura di Heisenberg:
#table(columns: (1fr, 1fr), align: center,
  [*Caso discreto*], [*Caso continuo*],
[
#set text(size: 10pt)
  $
[a_vb(k),a^dagger_vb(l)]=delta_(vb(k),vb(l))\
H=sum_vb(k) E_vb(k)(a^dagger_vb(k)a_vb(k)+1/2)
$
$
phi&=sum_vb(k)1/sqrt(2V E_vb(k))(a_vb(k)(t)e^(i vb(k)dot vb(x))+a^dagger_vb(k)(t)e^(-i vb(k)dot vb(x)))\
  &=sum_vb(k)1/sqrt(2V E_vb(k))(a_vb(k)e^(-i k dot x)+a^dagger_vb(k)e^(i k dot x))
$
$
a_vb(k)(t)=sqrt((E_vb(k))/(2V))integral dd(vb(x),3) e^(-i vb(k) dot vb(x))(phi+i/E_vb(k)dot(phi))\
a_vb(k)=sqrt((E_vb(k))/(2V))integral dd(vb(x),3) e^(+i k dot x)(phi+i/E_vb(k)dot(phi))
$
],
[
#set text(size: 10pt)
$
[a_vb(k),a^dagger_vb(l)]=delta(vb(k)-vb(l))\
H=integral dd(vb(k),3) E(vb(k))(a^dagger_vb(k)a_vb(k)+1/2 V/(2pi)^3)
$
$
phi&=integral dd(vb(k),3)1/((2pi)^(3\/2)sqrt(2 E_vb(k))){a_vb(k)(t)e^(i vb(k)dot vb(x))+a^dagger_vb(k)(t)e^(-i vb(k)dot vb(x))}\
  &=integral dd(vb(k),3)1/((2pi)^(3\/2)sqrt(2 E_vb(k))){a_vb(k)e^(-i k dot x)+a^dagger_vb(k)e^(i k dot x)}
$
$
a_vb(k)(t)=sqrt((E_vb(k))/(2(2pi)^3))integral dd(vb(x),3) e^(-i vb(k) dot vb(x))(phi+i/E_vb(k)dot(phi))\
a_vb(k)=sqrt((E_vb(k))/(2(2pi)^3))integral dd(vb(x),3) e^(+i k dot x)(phi+i/E_vb(k)dot(phi))
$
],
)
Propagatore di Feynman:
$
Delta_F (x-x')&=braket(0,bb(T)(phi(x),phi(x')),0)\
&=braket(0,theta(t-t')phi(x)phi(x')+theta(t'-t)phi(x')phi(x),0)
$
$
(square+m^2)Delta_F (x-x')=-i delta(x-x')
$
$
Delta_F (x-x')&=integral dd(vb(k),3)/((2pi)^3E_vb(k)) {
  theta(t-t')e^(-i k(x-x'))+theta(t'-t)e^(i k(x-x'))
  }\
&=integral dd(k,4)/(2pi)^4 (i e^(-i k dot (x - x')))/(k^2-m^2+i epsilon)
$
$
  Delta^E_F=-integral dd(k,4)/(2pi)^4 (e^(-i k dot (x - x')))/(k^2+m^2)
$

== Campo scalare complesso (coppia di campi scalari reali)
Teorie disaccoppiate per $phi_1, phi_2$. Introduciamo
$
phi=1/sqrt(2)(phi_1+i phi_2) wide overline(phi)=1/sqrt(2)(phi_1-i phi_2)\
cal(S)=integral dd(x,4) thin { diff_mu overline(phi) thin diff^mu phi
  - ((m c)/hbar)^2 overline(phi)phi}
$
$
  a(vb(k)) = ( a_1(vb(k)) + i a_2(vb(k)) )/sqrt(2) wide
& b(vb(k)) = ( a_1(vb(k)) - i a_2(vb(k)) )/sqrt(2)
\
  a^dagger (vb(k)) = ( a^dagger_1(vb(k)) - i a^dagger_2(vb(k)) )/sqrt(2) wide
& b^dagger (vb(k)) = ( a^dagger_1(vb(k)) + i a^dagger_2(vb(k)) )/sqrt(2)
$
$
  &phi = integral dd(vb(k),3)/sqrt((2pi)^3 2E_vb(k))
    {a(vb(k))e^(-i k dot x) + b^dagger (vb(k))e^(i k dot x)}\
  &overline(phi) = integral dd(vb(k),3)/sqrt((2pi)^3 2E_vb(k))
    {a^dagger(vb(k))e^(i k dot x) + b (vb(k))e^(-i k dot x)}
$
$
[a,b]=[a^dagger,b^dagger]=[a,b^dagger]=[a^dagger,b]=0\
[a,a^dagger]=[b,b^dagger]=delta\
:H:thin=integral dd(vb(k),3) E_vb(k) {a^dagger a(vb(k))+b^dagger b(vb(k))}\
:Q:thin=integral dd(vb(k),3) E_vb(k) {a^dagger a(vb(k))-b^dagger b(vb(k))}
$

== Teorie di Gauge
=== Premesse di Teoria dei Gruppi e delle Rappresentazioni
#align(center)[#table(columns: 4, inset: 10pt, align: center,
[*Algebra\ (con rango)*], [*Numero di generatori*], [*Gruppo*], [*Dimensione della Fondamentale*],
$frak(s u)(n+1)=A_n$  , $n^2+2n$     , $op("SL")(n+1,bb(C))$    , $n+1$,
$frak(s o)(2n+1)=B_n$ , $2n^2+n$     , $op("Spin")(2n+1,bb(C))$ , $2n+1$,
$frak(s p)(2n)=C_n$   , $2n^2+n$     , $op("Sp")(2n,bb(C))$     , $2n$,
$frak(s o)(2n)=D_n$   , $2n^2-n$     , $op("Spin")(2n,bb(C))$   , $2n$,
$E_n,thin n=6,7,8$    , $78,133,248$ , $op("E ")_n$             , $27,56,248$,
$F_4$                 , $52$         , $op("F ")_4$             , $14$,
$G_2$                 , $14$         , $op("G ")_2$             , $7$
)]
Qualche isomorfismo in basse dimensioni:
- $frak(s u)(2) tilde.equiv frak(s o)(3) tilde.equiv frak(s p)(2)$
  (pseudorealità del doppietto)
- $frak(s u)(4) tilde.equiv frak(s o)(6)$
- $frak(s p)(4) tilde.equiv frak(s o)(5)$
Notiamo che la fondamentale di $E_8$ coincide con l'aggiunta.

=== Yang-Mills
$
cal(D)_mu = diff_mu + (i q)/(hbar c) cal(A)_mu\
(i q)/(hbar c) cal(F)_(mu nu) =[cal(D)_mu,cal(D)_nu]=
(i q)/(hbar c) (diff_mu cal(A)_nu - diff_nu cal(A)_mu
+(i q)/(hbar c) [cal(A)_mu,cal(A)_nu]
)
$
Dato un campo di materia $Psi$, le trasformazioni di gauge sono:
$
  cases(
    Psi mapsto exp((i q)/(hbar c) tilde(Lambda)) Psi,
    cal(A)_mu mapsto exp((i q)/(hbar c) tilde(Lambda)) cal(A)_mu
    exp(-(i q)/(hbar c) tilde(Lambda))+
    i (hbar c)/q
    (diff_mu exp((i q)/(hbar c)tilde(Lambda))) exp(-(i q)/(hbar c) tilde(Lambda))
  )
$
o, introducendo $tilde(Omega)= exp((i q)/ (hbar c) tilde(Lambda))$,
$
  cases(
    Psi mapsto tilde(Omega) Psi,
    cal(A)_mu mapsto tilde(Omega) cal(A)_mu tilde(Omega)^(-1)
    + i (hbar c)/q (diff_mu tilde(Omega)) tilde(Omega)^(-1) =
    tilde(Omega) cal(A)_mu tilde(Omega)^(-1)
    - i (hbar c)/q tilde(Omega) diff_mu tilde(Omega)^(-1)
  )
$
Segue
$
  cal(D)_mu mapsto tilde(Omega) cal(D)_mu tilde(Omega)^(-1), #h(1em)
  cal(F)_(mu nu) mapsto tilde(Omega) cal(F)_(mu nu) tilde(Omega)^(-1), #h(1em)
  delta cal(A)_mu mapsto tilde(Omega) thin delta cal(A)_mu tilde(Omega)^(-1)
$
Indicando con $l_R$ l'indice della rappresentazione con normalizzazione
$tr[T^a T^b]=l_R delta_(a b)$ (per non avere ghosts, dobbiamo prendere le forme
compatte),
$
cal(S)&=integral dd(x,4) thin tr[ -1/(4l_R) cal(F)^(mu nu)cal(F)_(mu nu)-
1/(l_R) cal(J)_mu cal(A)^mu ]\
&=integral dd(x,4) thin { -1/4 F^(a,mu nu)F^a_(mu nu)-
J^a_mu A^(a,mu) }
$
Identità di Palatini:
$
  delta cal(F)_(mu nu) = [cal(D)_mu,delta cal(A)_nu] - [cal(D)_nu, delta cal(A)_mu]
$
Equazioni del moto:
$
[cal(D)_mu,cal(F)^(mu nu)]=cal(J)^nu, #h(1em)
underbrace([cal(D)_mu,tilde(cal(F))^(mu nu)]=0,"Identità di Bianchi (Jacobi)")
$
Le equazioni non sono esprimibili senza la connessione: questi bosoni sono carichi.

== Higgs Model
$
cal(S)&=integral dd(x,4) thin tr[
  -1/(4l_R) cal(F)^(mu nu)cal(F)_(mu nu)
  +abs(cal(D)_mu phi)^2-V(phi^dagger phi)
]
$
dove $abs(cal(D)_mu phi)^2
=(cal(D)_mu phi)^dagger cal(D)^mu phi
=(diff_mu overline(phi)^T-i q cal(A)_mu overline(phi)^T)(diff^mu phi+ i q cal(A)^mu phi)$.\
Per ogni generatore rotto spontaneamente, il bosone di Goldstone corrisponente viene mangiato
dal campo di gauge. Il corrisponente campo di forza $cal(F)^(i,mu nu)$ prende massa. Si veda
il caso abeliano.
=== Caso abeliano $U(1)$
Prendiamo per semplicità $V=lambda/4 (overline(phi) phi - rho^2)^2$.
Utile per superconduttività (vortici dei superconduttori di tipo II).\
Equazioni del moto:
$
cases(
  diff_mu F^(mu nu) = q(overline(phi)i cal(D)^nu phi-phi i overline(cal(D)^nu)overline(phi))
  =q(overline(phi)i diff^nu phi-phi i diff^nu overline(phi))-2q^2A^nu overline(phi)phi\
  cal(D)_mu cal(D)^mu phi + lambda/2 (overline(phi)phi-rho^2)phi = 0
)
$
Espandiamo attorno a $phi=rho+(xi+i eta)/sqrt(2)$.
$
cal(S)tilde.eq integral dd(x,4) thin {
  -1/4 F^(mu nu)F_(mu nu)
  +1/2 (diff_mu eta+sqrt(2)q A_mu rho)^2
  +1/2 diff_mu xi diff^mu xi-1/2(lambda rho^2)xi^2}
$
Sotto trasformazione di gauge infinitesime
$
  cases(
    A^mu mapsto A^mu -diff^mu theta\
    xi mapsto xi\
    eta mapsto eta + sqrt(2)q rho theta
  )
$
e quindi con $theta = - eta_0/(sqrt(2)q rho)$ si ha $eta = 0$.
$
cal(S)tilde.eq integral dd(x,4) thin {
  underbrace(
    -1/4 F^(mu nu)F_(mu nu)
    +q^2 rho^2 A_mu A^mu,
    "Lagrangiana di Proca"\ "(screening)"
  )
  +
  underbrace(
    1/2 diff_mu xi diff^mu xi-1/2(lambda rho^2)xi^2,
    "Particella massiva"
  )
  }
$

== Teoria di Stueckelberg
Teoria con campo di gauge massivo
$
  cal(L)=-1/4 F^(mu nu) F_(mu nu)+m^2/2 underbrace((diff_mu eta+A_mu)^2,"non omogenea in " eta)
$
con simmetria
$
  cases(
    delta A_mu = -diff_mu theta,
    delta eta = theta
  )
$

== Teorie di spin $1/2$
$
  Psi_L mapsto e^(i vb(sigma)/2 dot ( vb(theta)-i vb(phi.alt) ) ) Psi_L wide
  Psi_L^dagger mapsto Psi_L^dagger e^(-i vb(sigma)/2 dot ( vb(theta)+i vb(phi.alt) ) ) \
  Psi_R mapsto e^(i vb(sigma)/2 dot ( vb(theta)+i vb(phi.alt) ) ) Psi_R wide
  Psi_R^dagger mapsto  Psi_R^dagger e^(-i vb(sigma)/2 dot ( vb(theta)-i vb(phi.alt) ) ) \
$
*Massa di Dirac*
$
-cal(L)_D^((m)) = Psi_L^dagger Psi_R + Psi_R^dagger Psi_L
$
*Masse di Majorana*
$
-cal(L)_(M L)^((m)) = Psi_L^T sigma^2 Psi_L + Psi_L^dagger sigma^2 Psi_L^* \
-cal(L)_(M R)^((m)) = Psi_R^T sigma^2 Psi_R + Psi_R^dagger sigma^2 Psi_R^*
$

Entrambi i termini di massa sono definiti a meno di trasformazioni del tipo
$
cases(
  Psi_L mapsto alpha_L Psi_L,
  Psi_R mapsto alpha_R Psi_R
)
$

#text(size: 0.8*1em,[
#smallcaps(underline("Breve giustificazione")): $cal(L)$ (quindi il termine di massa) dev'essere un operatore
scalare, quindi deve stare nella rappresentazione $bb((0,0))$. Il termine di massa può essere
- quadratico in $Psi_L$;
- quadratico in $Psi_R$;
- prodotto di $Psi_L, Psi_R$.

I primi due casi sono analoghi, trattiamo il primo per semplicità. Sappiamo che
$
  bb((1/2,0))otimes bb((1/2,0)) = bb((1,0)) oplus bb((0,0))
$
e vogliamo capire come proiettare sul singoletto. Questa è una cosa che sappiamo fare bene:
$
  ket((0,0)) = (ket(0)ket(1)-ket(1)ket(0))/sqrt(2)
$
Dunque la nostra massa dev'essere proporzionale a
$
  Psi_L^arrow.t Psi_L^arrow.b - Psi_L^arrow.b Psi_L^arrow.t prop Psi_L^T sigma_2 Psi_L
$

Per il secondo caso, notiamo che $Psi_R^dagger in bb((1/2,0))$, dunque dobbiamo proiettare sul sottospazio degli
scalari di ${Psi_L}otimes{Psi_R^dagger}$. Per far questo, mettiamo l'algebra dell'azione su $Psi_R^dagger$ in
forma standard (uso la pseudorealità del doppietto):
$
  V_i (Psi_R^dagger)_a
  = - (sigma^i)^T_(a b) /2 (Psi_R^dagger)_b
  = sigma^2 sigma^i /2 sigma^2 (Psi_R^dagger)^T
  = sigma^2 sigma^i /2 sigma^2 Psi_R^*
$
Conviene quindi guardare la rappresentazione su $sigma^2 Psi_R^*$:
$
  V_i (sigma^2 Psi_R^*) = sigma^i / 2 (sigma^2 Psi_R^*)
$
Prendendo il sottospazio di singoletto di ${Psi_L}otimes{sigma^2 Psi_R^*}$, si ottiene che il termine di
massa è proporzionale a
$
  (sigma^2 Psi_R^*)^T sigma^2 Psi_L = Psi_R^dagger Psi_L
$
])

Per avere un termine cinetico scalare quadratico nei campi, dobbiamo prendere almeno una derivata prima.
Non ne prendiamo di più perché conosciamo la teoria solo nel limite IR.
Vogliamo quindi prendere lo scalare in $bb((1/2,1/2))otimes bb((1/2,0))otimes bb((0,1/2))$.
Per il singoletto su $L$, abbiamo, a meno di derivate totali, due opzioni:
$
sigma_(mu,arrow.t a) diff^mu Psi_(L arrow.b)-sigma_(mu,arrow.b a) diff^mu Psi_(L arrow.t)
prop sigma^2 sigma_mu^T sigma^2 diff^mu Psi_L \
sigma_(mu,arrow.t a) diff^mu (sigma^2 Psi_R^*)_arrow.b-sigma_(mu,arrow.b a) diff^mu
(sigma^2 Psi_R^*)_arrow.t
prop sigma^2 sigma_mu^T diff^mu Psi_R^*
$
Prendiamo ora il singoletto su $R$. Ci sono quattro possibilità:
$
Psi_L^dagger sigma^mu diff_mu sigma^2 Psi_R^* + h.c.\
Psi_L^dagger sigma^mu diff_mu Psi_L \
Psi_R^dagger overline(sigma)^mu diff_mu Psi_R \
Psi_R^dagger overline(sigma)^mu diff_mu sigma^2 Psi_L^*+h.c.
$
Il primo e il quarto sono essenzialmente lo stesso termine. Questi possono sempre essere eliminati da una
trasformazione del tipo
$
cases(
  Psi_L mapsto A Psi_L + B sigma_2 Psi_R^*,
  Psi_R mapsto C Psi_R + D sigma_2 Psi_L^*,
)
$
se non ci sono altri termini in $cal(L)$.

*Lagrangiane di Weyl*
$
  cal(L)_(W L) = i Psi_L^dagger sigma^mu diff_mu Psi_L wide
  cal(L)_(W R) = i Psi_R^dagger overline(sigma)^mu diff_mu Psi_R \
  sigma^mu diff_mu Psi_L =0 wide overline(sigma)^mu diff_mu Psi_R = 0 \
  cal(L)_(W L\/R) = i overline(Psi)(bb(1)mp gamma_5)/2 slashed(diff) Psi
$
Stanno nelle più semplici irreps del gruppo di Lorentz.

*Lagrangiana di Dirac*
La lagrangiana:
$
  cal(L)_D = cal(L)_(W L)+cal(L)_(W R)+m cal(L)^((m))_D
  = i overline(Psi) slashed(diff) Psi - m overline(Psi) Psi \
  i slashed(diff) Psi - m Psi = 0 wide (i slashed(diff) + m)(i slashed(diff) - m) Psi = -(square + m) Psi = 0
$
Base relativistica:
$
  Psi = vec(Psi_L, Psi_R) wide Psi^dagger = vecrow(Psi_L^dagger, Psi_R^dagger)
  wide overline(Psi)=Psi^dagger beta = vecrow(Psi_R^dagger, Psi_L^dagger)
$
Base non relativistica:
notiamo che per la relazione di dispersione $E^2=vb(p)^2+m^2$, su un'onda monocromatica vale
$
  Psi = pm gamma^0 (vb(gamma) dot vb(p)+m) / sqrt(vb(p)^2+m^2) Psi
$
Quindi i proiettori sulle configurazioni di particella e antiparticella sono rispettivamente
$
  Pi_(u\/d) = gamma^0 (gamma^0 sqrt(vb(p)^2+m^2) pm (vb(gamma) dot vb(p)+m)) / sqrt(vb(p)^2+m^2)
$
Come base, prendiamo quattro vettori che quando fermi ($vb(p)=0$) sono autostati dei proiettori:
$
cases(
  Psi_u &= 1/sqrt(2) (Psi_R+Psi_L),
  Psi_d &= 1/sqrt(2) (Psi_R-Psi_L),
)
$

*Lagrangiana della QED*
$
  cal(L) = i overline(Psi)slashed(cal(D)) Psi-m overline(Psi) Psi - 1/4 F^(mu nu) F_(mu nu)
$

*Fermioni di Majorana*
Sotto scambio particella #math.arrow.l.r antiparticella (coniugio di carica),
per l'evoluzione temporale $Psi^c = C overline(Psi)^T$,
ma la struttura tensoriale deve rimanere preservata, quindi $C gamma^T_(mu nu) = -gamma_(mu nu)C$.
Se $C^2=bb(1)$ e $C gamma_mu^T = -gamma_mu C$, la condizione è rispettata. Questo inoltre garantisce
che $overline(Psi) slashed(vb(x)) Psi$ cambi segno sotto coniugio.
$
  C gamma_mu C = -gamma_mu^T wide C=C^dagger=C^(-1)=-C^T=-C^* \
  Psi^c = C overline(Psi)^T = C beta Psi^* \
  overline(Psi)^c = - Psi^T C
$
$C$ anticommuta con le $gamma^mu$ simmetriche. Quindi a meno di fasi $C=gamma^0 gamma^2$.

Condizione di realità (prendere il coniugato è un'involuzione):
$
  Psi_R = pm sigma^2 Psi_L^* quad arrow.l.r.double quad Psi = pm Psi^c
$
(In $D=4$ è anche di Weyl, in $D=6,10$ no.)
Richiedono di “fattorizzare il gruppo” e avere un isomorfismo tra i vari fattori.
Nelle due basi:
$
  C_"Weyl" = sigma^3 otimes sigma^2 = mat(sigma^2,0;0,-sigma^2)
  wide C_"Dirac" = -sigma^1 otimes sigma^2 = mat(0,-sigma^2;-sigma^2,0)
$

Si noti che utilizzando le matrici $gamma_5, C$ si possono costruire questi ulteriori
termini di massa:
$
  -cal(L)_(D chi)^((m)) = i overline(Psi) gamma_5 Psi \
  cal(L)_(M -)^((m)) = -overline(Psi)^c Psi - overline(Psi) Psi^c = cal(L)_(M L)^((m))-cal(L)_(M R)^((m)) \
  cal(L)_(M +)^((m)) = overline(Psi)^c gamma_5 Psi - overline(Psi) gamma_5 Psi^c =
    cal(L)_(M L)^((m))+cal(L)_(M R)^((m)) \
$

=== Azione di Lorentz sugli spinori: trasformazioni continue
$
  Psi mapsto exp[ i dmat(
    vb(sigma)/2 dot (vb(theta)-i vb(phi.alt)),
    vb(sigma)/2 dot (vb(theta)+i vb(phi.alt))
    ) ] Psi &= exp(-1/4 omega^(mu nu) gamma_(mu nu)) Psi
$
$
  overline(Psi) mapsto overline(Psi) e^(1/4 omega^(mu nu) gamma^dagger_(mu nu))
$

*Altre rappresentazioni vettoriali delle $gamma$*
Per il termine di massa abbiamo preso il singoletto. Proviamo ora a costruire $bb((1/2,1/2))$.
$
  bb((1/2,1/2)) tilde Psi_L Psi_L^dagger tilde Psi_R Psi_R^dagger
$
Notiamo che $A^mu = 1/2 tr[(A^nu sigma_nu) overline(sigma)^mu]$. Dunque
$
  A^mu & tilde tr[Psi_L Psi_L^dagger overline(sigma)^mu] tilde Psi_L^dagger overline(sigma)^mu Psi_L \
       & tilde tr[Psi_R Psi_R^dagger sigma^mu] tilde Psi_R^dagger sigma^mu Psi_R \
$
ossia $overline(Psi) gamma^mu Psi$ è un vettore. Usando l'azione del gruppo di Lorentz sugli spinori (oppure
notando che con opportune contrazioni formano una quantità scalare), si
ottengono queste identificazioni:
- $overline(Psi) bb(1) Psi$ scalare
- $overline(Psi) gamma^mu Psi$ vettore
- $overline(Psi) gamma^(mu nu) Psi$ tensore antisimmetrico
- $overline(Psi) gamma^mu gamma_5 Psi$ pseudovettore
- $overline(Psi) gamma_5 Psi$ pseudoscalare

=== Azione di Lorentz sugli spinori: trasformazioni discrete
Le matrici $M=C,P,T$ le prendiamo t.c. $M=M^dagger=M^(-1)$, a meno di una fase. Per $*$ intendo
l'operatore antilineare di coniugazione.
#table(
  columns:(1fr,1fr,1fr),align:center,inset:1em,
  [*Coniugio di carica $cal(C)$*],[*Parità $cal(P)$*],[*Time-reversal $cal(T)$*],
$
   Psi mapsto Psi^c = C overline(Psi)^T = C beta Psi^* \
  C gamma_mu^T = -gamma_mu C \ therefore C tilde gamma^0 gamma^2 \
  x^mu mapsto x^mu \
  e "(QED coupling)"  mapsto -e \
  cal(L)_(W L) arrow.l.r cal(L)_(W R) \
  cal(L)_(M L)^((m)) arrow.l.r cal(L)_(M R)^((m)) \
  cal(L)_(D)^((m)) mapsto cal(L)_(D)^((m))
$,
$
  Psi mapsto P Psi \
  [P,gamma^0]={P,gamma^i}=0 \ therefore P tilde beta \
  x^0 mapsto x^0, x^i mapsto -x^i\ x^mu mapsto x_mu \
  e mapsto e \
  cal(L)_(W L) arrow.l.r cal(L)_(W R) \
  cal(L)_(M L)^((m)) arrow.l.r cal(L)_(M R)^((m)) \
  cal(L)_(D)^((m)) mapsto cal(L)_(D)^((m))
$,
$
  Psi mapsto T* Psi \
  {T,gamma^(0,2)}=[T,gamma^(i != 2)]=0 \
  therefore T tilde gamma^0gamma^2\
  x^0 mapsto -x^0, x^i mapsto x^i\
  x^mu mapsto -x_mu \
  e mapsto e \
  cal(L)_(W L\/R) mapsto cal(L)_(W L\/R) \
  cal(L)_(M L\/R)^((m)) mapsto cal(L)_(M L\/R)^((m)) \
  cal(L)_(D)^((m)) mapsto cal(L)_(D)^((m))
$
)
Questo rispetta il teorema $cal(C P T)$ (ogni lagrangiana è invariante sotto $cal(C P T)$).
